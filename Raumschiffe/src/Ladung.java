
public class Ladung {
	// Attribute
	private String Frachtname;
	private int Anzahl;

	// Konstrukor
	public Ladung() {
	}

	public Ladung(String Frachtname, int Anzahl) {
		this.Frachtname = Frachtname;
		this.Anzahl = Anzahl;
	}

	// setter
	public void setFrachtname(String name) {
		this.Frachtname = name;
	}

	public void setAnzahl(int zahl) {
		this.Anzahl = zahl;
	}

	// getter
	public int getAnzahl() {
		return this.Anzahl;
	}

	public String getBezeichnung() {
		return this.Frachtname;
	}

}
