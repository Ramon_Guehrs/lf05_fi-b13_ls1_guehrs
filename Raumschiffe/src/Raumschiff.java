import java.util.ArrayList;

public class Raumschiff {
	// Attribute
	private String name;
	private double schutzschild;
	private double energie;
	private double vitalsysteme;
	private double huelle;
	private int photonentorpedos;
	private int reperaturandroid;
	
	private ArrayList<Ladung> ladungsverzeichnis = new ArrayList();

	// Raumschiffe
	 public Raumschiff() {}
	  
	  public Raumschiff(String name, double schutzschild, double energie, 
	  double vitalsysteme,double huelle,int photonentorpedos,int reperaturandroid) { 
		  
	    this.name = name; 
	    this.schutzschild = schutzschild;
	    this.energie = energie;
	    this.vitalsysteme = vitalsysteme;
	    this.huelle = huelle;
	    this.photonentorpedos = photonentorpedos;
	    this.reperaturandroid = reperaturandroid;
	  }

	// getter
	

	public String getName() {
		return this.name;
	}

	public double getSchutzschild() {
		return this.schutzschild;
	}

	public double getEnergie() {
		return this.energie;
	}

	public double getVitalsysteme() {
		return this.vitalsysteme;
	}

	public double getHuelle() {
		return this.huelle;
	}

	public int getPhotonentorpedos() {
		return this.photonentorpedos;
	}

	public int getReperaturandroid() {
		return this.reperaturandroid;
	}

	
	// setter
	public void setName(String name) {
		this.name = name;
	}
	public void setSchutzschild(double setschutzschild) {
		this.schutzschild = setschutzschild;
	}

	public void setEnergie(double energie) {
		this.energie = energie;
	}

	public void setVitalsysteme(double vitalsysteme) {
		this.vitalsysteme = vitalsysteme;
	}

	public void setHuelle(double huelle) {
		this.huelle = huelle;
	}

	public void setPhotonentorpedos(int photonentorpedos) {
		this.photonentorpedos = photonentorpedos;
	}

	public void setReperaturandroid(int reperaturandroid) {
		this.reperaturandroid = reperaturandroid;
	}
	
	// methoden

	public void zustand() {
		   System.out.println("-- Stastuswerte --");
		    System.out.println("Raumschiffsname: "+ name);
		    System.out.println("Schutzschildlevel = " + schutzschild);
		    System.out.println("Energielevel: " + this.getEnergie());
		    System.out.println("Vitalsystemelevel: " + this.getVitalsysteme());
		    System.out.println("Huellenlevel: " + this.getHuelle());
		    System.out.println("Photonentorpedos: " + this.getPhotonentorpedos());
		    System.out.println("aktive Reperaturandroid: " + this.getReperaturandroid());
		    System.out.println("---");

	}

	public void addLadung(Ladung l1) {
		ladungsverzeichnis.add(l1);
	}
	
	public void photonentorpedosabschiessen(int photonentorpedos ) { 
    if (this.photonentorpedos <= 0) {
    	System.out.println ("click");
    	
    } else {
    	this.photonentorpedos--;
    }
	}
    public void phaserkanone(double energie ) {
    	this.energie = energie * 0.5;
    	if (energie < 0.5 ) {
    	System.out.println ("click");
    	
    	}
    }
    	public void treffer() {
    		System.out.print( name +"wurde getroffen!");
    	}
    
	}

