public class Besitzer {
  private String vorname;
  private String nachname;
  private Konto k1;
  private Konto k2;
  
  // Konstrukor
  public Besitzer() {}
  
  public Besitzer(String vorname, String nachname, Konto k1, Konto k2) { 
    this.vorname = vorname; 
    this.nachname = nachname;
    this.k1 = k1;
    this.k2 = k2;
  }
  public Besitzer(String vorname, String nachname, Konto k1) {
	  this.vorname = vorname;
	  this.nachname = nachname;
	  this.k1 = k1;
  }
  
  // setter
  public void setVorname(String name) { this.vorname = name; }
  
  public void setNachname(String name) { this.nachname = name; }
  
  public void setKonto1(Konto eingabe) { this.k1 = eingabe; }
  
  public void setKonto2(Konto eingabe) { this.k2 = eingabe; }
  
  // getter
  public String getVorname() { return this.vorname; }
  
  public String getNachname() { return this.nachname; }
  
  public Konto getKonto1() { return this.k1; }
  
  public Konto getKonto2() { return this.k2; }

  // Methoden
  public void gesamtUebersicht() { 
    System.out.println("Konto 1");
    System.out.println("IBAN: " + k1.getIban());
    System.out.println("Nr.: " + k1.getKontonr());
    System.out.println("Balance: " + k1.getKontostand() + "� \n");
    
    System.out.println("Konto 2");
    System.out.println("IBAN: " + k2.getIban());
    System.out.println("Nr.: " + k2.getKontonr());
    System.out.println("Balance: " + k2.getKontostand() + "� \n");
  }
  
  public double gesamtGeld() { return (k1.getKontostand() + k2.getKontostand()); }

}
