
public class Test {

  public static void main(String[] args) {
    // Objekte instanzen erstellen
    Konto k1 = new Konto();
    Konto k2 = new Konto("DE500", 0002, 100.19);
    Konto Ramons_Konto = new Konto ( "DE437708687354323",  1 , 420.69) ;
    Besitzer Ramon = new Besitzer("Ramon",  "G�hrs",  Ramons_Konto);

    Besitzer b1 = new Besitzer();

    // Objektmanipulierung
    k1.setIban("DE499");
    k1.setKontonr(0001);
    k1.setKontostand(10.99);
    
    b1.setKonto1(k1);
    b1.setKonto2(k2);
    
    // Methoden Test
    k1.geldUeberweisen(30.00, k2);

    // Konto Ausgaben
    System.out.println("-- Konto Ausgaben --");
    System.out.println("IBAN: " + k1.getIban());
    System.out.println("K NR: " + k1.getKontonr());
    System.out.println("STAND: " + k1.getKontostand());
    
    System.out.println("---");

    System.out.println("IBAN: " + k2.getIban());
    System.out.println("K NR: " + k2.getKontonr());
    System.out.println("STAND: " + k2.getKontostand());
    
    // Besitzer Ausgaben
    System.out.println("-- besitzer Ausgaben --");
    
    b1.gesamtUebersicht();
    System.out.println("Gesamt: " + b1.gesamtGeld() + "�");
    
    //Besitzer
Ramon.gesamtUebersicht();


      }

}
